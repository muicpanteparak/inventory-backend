FROM openjdk:8-alpine
ARG JAR_APP
ENV JAR_APP=${JAR_APP}
EXPOSE 8080
WORKDIR /app
ADD target/${JAR_APP} .
CMD java -jar ${JAR_APP}