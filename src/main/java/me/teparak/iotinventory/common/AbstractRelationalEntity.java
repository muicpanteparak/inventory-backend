package me.teparak.iotinventory.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class AbstractRelationalEntity implements Serializable {

    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private Date createdDate;

    @NotNull
    private Date lastModifiedDate;

    @Override
    public String toString() {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @PrePersist
    public void onCreate(){
        createdDate = new Date();
        lastModifiedDate = createdDate;
    }

    @PreUpdate
    public void onUpdate(){
       lastModifiedDate = new Date();
    }

}
