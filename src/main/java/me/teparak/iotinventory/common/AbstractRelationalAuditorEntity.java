package me.teparak.iotinventory.common;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import me.teparak.iotinventory.security.user.UserAccount;
import me.teparak.iotinventory.security.user.UserPrincipal;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractRelationalAuditorEntity extends AbstractRelationalEntity {

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private UserAccount createdBy;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private UserAccount modifiedBy;

    @PrePersist
    public void onSave() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null) modifiedBy = createdBy = null;
        else {
            modifiedBy = createdBy = new UserAccount(((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUid());
        }
    }

    @PreUpdate
    public void onUpdate() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        if (auth == null) modifiedBy = null;
        else {
            modifiedBy = new UserAccount(((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUid());
        }
    }
}
