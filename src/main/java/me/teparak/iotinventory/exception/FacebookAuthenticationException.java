package me.teparak.iotinventory.exception;

import org.springframework.security.core.AuthenticationException;

public class FacebookAuthenticationException extends AuthenticationException {
    public FacebookAuthenticationException(String msg, Throwable t) {
        super(msg, t);
    }

    public FacebookAuthenticationException(String msg) {
        super(msg);
    }
}
