package me.teparak.iotinventory.security;

import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;

@Data
@Configuration
@ConfigurationProperties("server")
public class CORSFilter {
    private String origin;

    @PostConstruct
    public void validate(){
        if (origin == null || StringUtils.isAnyBlank(origin)){
            throw new IllegalArgumentException("CORS is not declared");
        }
    }

    @Bean
    public WebMvcConfigurer CORSConfigurer() {
        return new WebMvcConfigurerAdapter() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry
                        .addMapping("/**")
                        .allowCredentials(true)
                        .allowedMethods("*")
                        .allowedHeaders("Authorization", "Content-Type")
                        .allowedOrigins(getOrigin());
            }
        };
    }
}
