package me.teparak.iotinventory.security.facebook;

import org.springframework.security.authentication.AbstractAuthenticationToken;


public class AuthenticationToken extends AbstractAuthenticationToken {
    private String token;

    public AuthenticationToken(String token) {
        super(null);
        this.token = token;
        this.setAuthenticated(false);
    }

    @Override
    public Object getCredentials() {
        return this.token;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }
}
