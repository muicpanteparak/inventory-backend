package me.teparak.iotinventory.security.facebook;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;


@EnableWebSecurity
@EnableGlobalAuthentication
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class FacebookWebSecurityConfigurer extends WebSecurityConfigurerAdapter {

    private final FacebookAuthenticationProvider authenticationProvider;
    private final FacebookAuthenticationFailureHandler failureHandler;
    private final FacebookAuthenticationEntrypoint authEntry;

    private final String[] permitURI = {
            "/inventory"
    };

    @Autowired
    public FacebookWebSecurityConfigurer(FacebookAuthenticationProvider authenticationProvider, FacebookAuthenticationFailureHandler failureHandler, FacebookAuthenticationEntrypoint autheEntrypoint) {
        this.authenticationProvider = authenticationProvider;
        this.failureHandler = failureHandler;
        this.authEntry = autheEntrypoint;
    }

    protected FacebookAuthenticationProcessingFilter facebookAuthenticationProcessingFilter() throws Exception {
        FacebookAuthenticationProcessingFilter facebookAuthenticationProcessingFilter = new FacebookAuthenticationProcessingFilter(this.failureHandler, new RequestFilter(permitURI));
        facebookAuthenticationProcessingFilter.setAuthenticationManager(super.authenticationManager());
        return facebookAuthenticationProcessingFilter;
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                .antMatchers(permitURI).permitAll()
                .anyRequest()
                .authenticated();

        http
                .cors()
                .and()
                .csrf()
                .disable()
                .exceptionHandling().authenticationEntryPoint(authEntry);

        http
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http
                .cors();

        http
                .addFilterBefore(facebookAuthenticationProcessingFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider);
    }
}
