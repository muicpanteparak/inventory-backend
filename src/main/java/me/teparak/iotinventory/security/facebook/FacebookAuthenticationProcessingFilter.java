package me.teparak.iotinventory.security.facebook;

import me.teparak.iotinventory.exception.FacebookAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FacebookAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {

    private final String HEADER_PREFIX = "Bearer ";
    private final FacebookAuthenticationFailureHandler authFailure;


    // TODO: Rewrite with AntPathMatcher
    public FacebookAuthenticationProcessingFilter(FacebookAuthenticationFailureHandler failureHandler, RequestMatcher requiresAuthenticationRequestMatcher) {
        super(requiresAuthenticationRequestMatcher);
        this.authFailure = failureHandler;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        String auth = request.getHeader("Authorization");

        if (auth == null)
            throw new FacebookAuthenticationException("Invalid Authentication Token");
//            return getAuthenticationManager().authenticate(new AuthenticationToken(""));

        String token = auth.substring(HEADER_PREFIX.length(), auth.length());
        return getAuthenticationManager().authenticate(new AuthenticationToken(token));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        context.setAuthentication(authResult);
        SecurityContextHolder.setContext(context);
        chain.doFilter(request, response);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {
        this.authFailure.onAuthenticationFailure(request, response, failed);
    }
}
