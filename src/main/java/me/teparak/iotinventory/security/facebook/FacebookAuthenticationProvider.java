package me.teparak.iotinventory.security.facebook;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.teparak.iotinventory.exception.FacebookAuthenticationException;
import me.teparak.iotinventory.security.user.UserPrincipal;
import me.teparak.iotinventory.security.user.UserService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

@Component
public class FacebookAuthenticationProvider implements AuthenticationProvider {
    @Autowired
    private FacebookAppCredentialConfiguration appConfiguration;

    @Autowired
    private UserService userService;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String token = (String) authentication.getCredentials();

        if (StringUtils.isAnyBlank(token)){
            throw new FacebookAuthenticationException("Token is not supplied");
        }

        ImmutablePair<AppResponse, UserInfoResponse> pair = sendRequest(token);
        AppResponse appResponse = pair.getLeft();
        UserInfoResponse userInfoResponse = pair.getRight();

//        System.out.println(String.format("AppID: %s, Returned: %s", appConfiguration.getAppId(), appResponse.getId()));
        if (!appResponse.getId().equals(appConfiguration.getAppId())){
            System.out.println(appResponse);
            throw new FacebookAuthenticationException("This user is not associate with this app");
        }


        String id = userInfoResponse.getId();
        String fname = userInfoResponse.getFirstname();
        String lname = userInfoResponse.getLastname();
        String name = userInfoResponse.getDisplayName();

        UserPrincipal principal = userService.createOrGetUser(id, fname, lname, name);

        return new UsernamePasswordAuthenticationToken(principal, null, principal.getAuthorities());
    }

    private ImmutablePair<AppResponse, UserInfoResponse> sendRequest(String token) throws FacebookAuthenticationException {
        String url = "https://graph.facebook.com/v2.11/?include_headers=false";
        String requestPayload = "{\"batch\":[{\"method\":\"GET\",\"relative_url\":\"app\"},{\"method\":\"GET\",\"relative_url\":\"me?fields=first_name,last_name,name,id\"}]}";

        HttpURLConnection con = null;
        try {
            URL link = new URL(url);
            con = (HttpURLConnection) link.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("charset", Charset.defaultCharset().name());
            con.setRequestProperty("Authorization", "Bearer " + token);
            con.setDoOutput(true);

            IOUtils.write(requestPayload, con.getOutputStream(), Charset.defaultCharset());

            int responseCode = con.getResponseCode();

            if (!HttpStatus.valueOf(responseCode).is2xxSuccessful()) {
                throw new FacebookAuthenticationException(con.getHeaderField("WWW-Authenticate"));
            }

            String payload = IOUtils.toString(con.getInputStream(), Charset.defaultCharset());
            Content[] responseMapper = new ObjectMapper().readValue(payload, Content[].class);

            if (responseMapper.length != 2){
                throw new FacebookAuthenticationException("Invalid response payload");
            }

            AppResponse appResponse = new ObjectMapper().readValue(responseMapper[0].getBody(), AppResponse.class);
            UserInfoResponse userInfoResponse = new ObjectMapper().readValue(responseMapper[1].getBody(), UserInfoResponse.class);

            return ImmutablePair.of(appResponse, userInfoResponse);
        } catch (MalformedURLException e) {
            throw new FacebookAuthenticationException(e.getMessage(), e.getCause());
        } catch (IOException e) {
            // unable to open con etc
            throw new FacebookAuthenticationException(e.getMessage(), e.getCause());
        } finally {
            if (con != null){
                IOUtils.close(con);
            }
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class AppResponse {
        private String id;

        public AppResponse(@JsonProperty("id") String id) {
            this.id = id;
        }

        public String getId() {
            return id;
        }

        @Override
        public String toString() {
            return "AppResponse{" +
                    "id='" + id + '\'' +
                    '}';
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class UserInfoResponse {
        private String firstname;
        private String lastname;
        private String displayName;
        private String id;

        public UserInfoResponse(@JsonProperty("first_name") String firstname, @JsonProperty("last_name") String lastname, @JsonProperty("name") String displayName, @JsonProperty("id") String id) {
            this.firstname = firstname;
            this.lastname = lastname;
            this.displayName = displayName;
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public String getDisplayName() {
            return displayName;
        }

        public String getId() {
            return id;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Content {
        private int code;
        private String body;

        public Content(@JsonProperty("code") int code, @JsonProperty("body") String body) {
            this.code = code;
            this.body = body;
        }

        public int getCode() {
            return code;
        }

        public String getBody() {
            return body;
        }
    }
}
