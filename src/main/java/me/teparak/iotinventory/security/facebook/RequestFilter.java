package me.teparak.iotinventory.security.facebook;

import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class RequestFilter implements RequestMatcher {

    private Set<String> set;
    protected RequestFilter(String[] array) {
        set = new HashSet<>(Arrays.asList(array));
    }

    @Override
    public boolean matches(HttpServletRequest request) {
        boolean contains = set.contains(request.getServletPath());
        return !contains;
    }
}
