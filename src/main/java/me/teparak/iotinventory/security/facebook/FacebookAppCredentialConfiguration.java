package me.teparak.iotinventory.security.facebook;

import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
@ConfigurationProperties(value = "facebook", ignoreUnknownFields = false)
public class FacebookAppCredentialConfiguration {
    private Long appId;
    private String appSecret;

    @PostConstruct
    public void validate(){
        if (StringUtils.isBlank(appId.toString()) || StringUtils.isEmpty(appId.toString())){
            throw new IllegalArgumentException("Facebook App ID is not declared");
        }

        if (StringUtils.isBlank(appSecret) || StringUtils.isEmpty(appSecret)){
            throw new IllegalArgumentException("Facebook App Secret is not declared");
        }
    }

    public String getAppId() {
        return appId.toString();
    }
    public void setAppId(Long appId) {
        this.appId = appId;
    }
    public String getAppSecret() {
        return appSecret;
    }
    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }
}
