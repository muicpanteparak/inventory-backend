package me.teparak.iotinventory.security.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class AuditorAwareImpl implements AuditorAware<UserAccount> {
//    @Override
//    public UserPrincipal getCurrentAuditor() {
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//
//        if (authentication == null || !authentication.isAuthenticated()) {
//            return null;
//        }
//
//        return ((UserPrincipal) authentication.getPrincipal());
//    }

    @Autowired
    private UserService userService;

    @Override
    public UserAccount getCurrentAuditor() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            return null;
        }
        UserPrincipal principal = ((UserPrincipal) authentication.getPrincipal());

        return userService.findByFacebookId(principal.getFacebookId());
    }

    @Bean
    AuditorAware<UserAccount> auditorProvider() {
        return new AuditorAwareImpl();
    }
}
