package me.teparak.iotinventory.security.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

@Data
public class UserPrincipal implements UserDetails {

    @JsonIgnore
    private Long uid;
    private String facebookId;
    private String firstname;
    private String lastname;
    private String displayName;
    private UserAccount.Role role;

    public UserPrincipal(UserAccount account) {
        this.uid = account.getId();
        this.facebookId = account.getFacebookId();
        this.firstname = account.getFirstname();
        this.lastname = account.getLastname();
        this.displayName = account.getDisplayname();
        this.role = account.getRole();
    }

    public UserPrincipal(Long uid, String facebookId, String firstname, String lastname, String displayName, UserAccount.Role role) {
        this.uid = uid;
        this.facebookId = facebookId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.displayName = displayName;
        this.role = role;
    }

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableCollection(Collections.singleton(role));
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return facebookId;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true; // not expired
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true; // not locked
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true; // not exp
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true; // enabled
    }
}
