package me.teparak.iotinventory.security.user;

import me.teparak.iotinventory.exception.RecordNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserAccount findByFacebookId(String id){
        return userRepository.findByFacebookId(id);
    }

    public UserPrincipal createOrGetUser(UserPrincipal principal){
        return createOrGetUser(principal.getFacebookId(), principal.getFirstname(), principal.getLastname(), principal.getDisplayName());
    }

    public UserPrincipal createOrGetUser(String userid, String firstname, String lastname, String displayName){

        UserAccount account = userRepository.findByFacebookId(userid);

        if (account == null) {
            UserAccount create = new UserAccount(userid, firstname, lastname, displayName);
            account = userRepository.save(create);
        }

        if (!account.getFirstname().equals(firstname) || !account.getLastname().equals(lastname) || !account.getDisplayname().equals(displayName)){
            account.setFirstname(firstname);
            account.setLastname(lastname);
            account.setDisplayname(displayName);

            userRepository.save(account);
        }

        return new UserPrincipal(account);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAccount account = userRepository.findByFacebookId(username);
        if (account == null)
            throw new UsernameNotFoundException("Username cannot be found");
        return new UserPrincipal(account);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    protected List<UserPrincipal> findAllUsers(){
        return userRepository.findAll().stream().map(UserPrincipal::new).collect(Collectors.toList());
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    protected UserPrincipal findUser(String facebookId) throws RecordNotFoundException {
        UserAccount account = userRepository.findByFacebookId(facebookId);
        if (account == null) throw new RecordNotFoundException("User not exist");
        return new UserPrincipal(account);
    }

    public UserPrincipal getMyPrincipal(){
        return ((UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
    }
}
