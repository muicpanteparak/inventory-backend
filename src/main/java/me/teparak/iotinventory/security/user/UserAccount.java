package me.teparak.iotinventory.security.user;

import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.teparak.iotinventory.common.AbstractRelationalEntity;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@Entity(name="UserAccount")
@NoArgsConstructor(access= AccessLevel.PRIVATE)
@Table(indexes = {@Index(columnList = "facebookId", unique = true)})
public class UserAccount extends AbstractRelationalEntity {

    @NotNull
    private String facebookId;

    @NotNull
    private String firstname;

    @NotNull
    private String lastname;

    @NotNull
    private String displayname;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Role role;

    public UserAccount(Long uid){
        super.setId(uid);
    }

    public UserAccount(String facebookId, String firstname, String lastname, String displayname) {
        this.facebookId = facebookId;
        this.firstname = firstname;
        this.lastname = lastname;
        this.displayname = displayname;
        this.role = Role.FACEBOOK_USER;
    }

    public enum Role implements GrantedAuthority {
        FACEBOOK_USER, ADMIN;

        @Override
        @JsonValue
        public String toString() {
            return "ROLE_" + super.name();
        }

        @Override
        public String getAuthority() {
            return super.toString();
        }
    }
}
