package me.teparak.iotinventory.security.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserControllerImpl {

    @Autowired
    private UserService userService;

    @GetMapping("/whoami")
    public ResponseEntity<?> whoami(Authentication auth){
        return ResponseEntity.ok((UserPrincipal)auth.getPrincipal());
    }

    @GetMapping
    public ResponseEntity<?> listUsers(){
        return ResponseEntity.ok(userService.findAllUsers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable() Long id){
        return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).build();
    }
}
