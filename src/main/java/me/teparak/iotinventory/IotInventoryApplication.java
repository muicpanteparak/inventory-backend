package me.teparak.iotinventory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.bohnman.squiggly.Squiggly;
import com.github.bohnman.squiggly.web.RequestSquigglyContextProvider;
import com.github.bohnman.squiggly.web.SquigglyRequestFilter;
import com.google.common.collect.Iterables;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.filter.ShallowEtagHeaderFilter;

import javax.servlet.http.HttpServletRequest;

@SpringBootApplication
@EnableCaching
public class IotInventoryApplication {

	@Bean
	public FilterRegistrationBean squigglyRequestFilter() {
		FilterRegistrationBean filter = new FilterRegistrationBean();
		filter.setFilter(new SquigglyRequestFilter());
		filter.setOrder(1);
		return filter;
	}

	public static void main(String[] args) {
		ConfigurableApplicationContext context = SpringApplication.run(IotInventoryApplication.class, args);
		Iterable<ObjectMapper> objectMappers = context.getBeansOfType(ObjectMapper.class)
				.values();
		Squiggly.init(objectMappers, new RequestSquigglyContextProvider());
		ObjectMapper objectMapper = Iterables.getFirst(objectMappers, null);
		if (objectMapper != null) {
			for (MappingJackson2HttpMessageConverter converter : context.getBeansOfType(MappingJackson2HttpMessageConverter.class).values()) {
				converter.setObjectMapper(objectMapper);
			}
		}
	}
}
