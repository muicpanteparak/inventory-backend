package me.teparak.iotinventory.inventory;

import me.teparak.iotinventory.exception.RecordNotFoundException;
import me.teparak.iotinventory.inventory.projection.InventoryProjection;
import me.teparak.iotinventory.inventory.projection.ListAllInventoryProjection;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class InventoryService {

    @Autowired
    private InventoryRepository inventoryRepository;

    @PreAuthorize("hasAuthority('ADMIN')")
    protected Inventory createNewInventory(@NotNull String name, @NotNull Double price, @NotNull Boolean isShow) throws IllegalArgumentException {

        if (StringUtils.isAnyEmpty(name)){
            throw new IllegalArgumentException("Name is empty");
        }

        if (price == null || price < 0){
            throw new IllegalArgumentException("Price cannot be < 0 or null");
        }

        if (isShow == null){
            throw new IllegalArgumentException("show cannot be null");
        }

        Inventory inventory = new Inventory();
        inventory.setName(name);
        inventory.setIsShow(isShow);
        inventory.setPrice(price);
        return inventoryRepository.save(inventory);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    protected Inventory adjustment(Long id, String name, Double price, Boolean show) throws RecordNotFoundException {
        Inventory inventory = searchInventory(id);

        if (name != null){
            inventory.setName(name);
        }

        if (price != null){
            inventory.setPrice(price);
        }

        if (show != null){
            inventory.setIsShow(show);
        }

        return inventory;
    }

    protected Inventory searchInventory(Long id) throws RecordNotFoundException {
        return inventoryRepository.findOne(id);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    protected List<ListAllInventoryProjection> searchAllInventory(){
        return inventoryRepository.findAllByIsShowIsNotNull(ListAllInventoryProjection.class);
    }

    protected List<InventoryProjection> searchShowInventory(){
        return inventoryRepository.findAllByIsShowTrue(InventoryProjection.class);
    }

    private Inventory save(Inventory inventory){
        return inventoryRepository.save(inventory);
    }
}
