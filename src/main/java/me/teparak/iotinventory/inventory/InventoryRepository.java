package me.teparak.iotinventory.inventory;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventoryRepository extends JpaRepository<Inventory, Long> {

    <T> List<T> findAllByIsShowTrue(Class<T> projection);
    <T> List<T> findAllByIsShowIsNotNull(Class<T> projection);

}
