package me.teparak.iotinventory.inventory.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface UsageProjection {

    @Value("#{target.getCreatedBy().getFacebookId()}")
    String getFacebookId();

    @Value("#{target.getCreatedBy().getDisplayname()}")
    String getDisplayname();

    @Value("#{target.inventory.name}")
    String getInventoryName();

    @Value("#{target.broughtPrice}")
    Double getBroughtPrice();

    @Value("#{target.qty}")
    Long getQuantity();

    @Value("#{target.getLastModifiedDate()}")
    Date getModifiedDate();

    @Value("#{target.getCreatedDate()}")
    Date getCreatedDate();

}
