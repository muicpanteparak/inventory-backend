package me.teparak.iotinventory.inventory.projection;

import org.springframework.beans.factory.annotation.Value;

public interface InventoryProjection {

    @Value("#{target.id}")
    Long getId();

    @Value("#{target.name}")
    String getName();

    @Value("#{target.price}")
    Double getPrice();

    @Value("#{target.isShow}")
    Boolean getIsShow();
}
