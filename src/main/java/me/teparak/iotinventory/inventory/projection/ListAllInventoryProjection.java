package me.teparak.iotinventory.inventory.projection;

import org.springframework.beans.factory.annotation.Value;

import java.util.Date;

public interface ListAllInventoryProjection {
    @Value("#{target.id}")
    Long getId();

    @Value("#{target.name}")
    String getName();

    @Value("#{target.price}")
    Double getPrice();

    @Value("#{target.isShow}")
    Boolean getShow();

    @Value("#{target.getCreatedBy().displayname}")
    String getCreatedBy();

    @Value("#{target.getModifiedBy().displayname}")
    String getModifiedBy();

    @Value("#{target.getCreatedDate()}")
    Date getCreatedDate();

    @Value("#{target.getLastModifiedDate()}")
    Date getModifiedDate();
}
