package me.teparak.iotinventory.inventory;

import me.teparak.iotinventory.exception.RecordNotFoundException;
import me.teparak.iotinventory.inventory.projection.UsageProjection;
import me.teparak.iotinventory.security.user.UserAccount;
import me.teparak.iotinventory.security.user.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.List;

@Service
public class InventoryTransactionService {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private InventoryUsageLogRepository inventoryUsageLogRepository;

    @PreAuthorize("hasAnyAuthority('ADMIN', 'FACEBOOK_USER')")
    protected InventoryUsageLog buy(@NotNull Long inventoryId, @NotNull Long qty) throws RecordNotFoundException, IllegalArgumentException {
        Inventory inventory;

        if (qty == null || qty <= 0){
            throw new IllegalArgumentException("quantity must be > 0");
        }

        if (inventoryId == null || (inventory = inventoryService.searchInventory(inventoryId)) == null){
            throw new RecordNotFoundException("Inventory id not found: " + inventoryId);
        }

        InventoryUsageLog log = new InventoryUsageLog();
        log.setInventory(inventory);
        log.setQty(qty);
        log.setBroughtPrice(inventory.getPrice());
        log = inventoryUsageLogRepository.save(log);
        inventory.addUsageLog(log);
        return inventoryUsageLogRepository.findOne(log.getId());
    }

    @PreAuthorize("hasAnyAuthority('ADMIN', 'FACEBOOK_USER')")
    protected List<InventoryUsageLog> myUsage(UserPrincipal principal){
        UserAccount account = new UserAccount(principal.getUid());
        return inventoryUsageLogRepository.findByCreatedBy(account, InventoryUsageLog.class);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    protected List<UsageProjection> listAllUsage(){
        return inventoryUsageLogRepository.findAllByCreatedByIsNotNull(UsageProjection.class);
    }
}
