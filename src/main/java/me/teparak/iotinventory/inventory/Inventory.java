package me.teparak.iotinventory.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.teparak.iotinventory.common.AbstractRelationalAuditorEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Data
@Entity
@EqualsAndHashCode(callSuper = false)
@Table(indexes = {@Index(columnList = "name"), @Index(columnList = "isShow")})
public class Inventory extends AbstractRelationalAuditorEntity {

    @NotNull
    @NotBlank
    private String name;

    private Double price;

    @NotNull
    @JsonIgnore
    private Boolean isShow;

    @JsonIgnore
    @OneToMany(mappedBy = "inventory", cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH}, orphanRemoval = true)
    private List<InventoryUsageLog> usageLogs;

    @Transactional
    public boolean addUsageLog(InventoryUsageLog log){
        synchronized (this){
            if (getUsageLogs() == null)
                setUsageLogs(new ArrayList<>());
        }
        return this.getUsageLogs().add(log);
    }

    @Transactional
    public boolean addBatchUsageLog(Collection<InventoryUsageLog> logs){
        synchronized (this) {
            if (getUsageLogs() == null)
                setUsageLogs(new ArrayList<>());
        }
        return this.getUsageLogs().addAll(logs);
    }
}
