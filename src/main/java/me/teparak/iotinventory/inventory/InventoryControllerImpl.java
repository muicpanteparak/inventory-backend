package me.teparak.iotinventory.inventory;

import me.teparak.iotinventory.exception.RecordNotFoundException;
import me.teparak.iotinventory.security.user.UserPrincipal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;


@RequestMapping("/inventory")
@RestController
public class InventoryControllerImpl {

    @Autowired
    private InventoryService inventoryService;

    @Autowired
    private InventoryTransactionService inventoryTransactionService;

    @GetMapping
    public ResponseEntity<?> getInventories() {
        return ResponseEntity.ok(inventoryService.searchShowInventory());
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllInventories() {
        return ResponseEntity.ok(inventoryService.searchAllInventory());
    }

    @PostMapping("/create")
    public ResponseEntity<?> create(@RequestParam("name") String name, @RequestParam("price") Double price, @RequestParam(value = "show", defaultValue = "true") Boolean show) {
        return ResponseEntity.status(HttpStatus.CREATED).body(inventoryService.createNewInventory(name, price, show));
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> findById(@PathVariable("id") Long id) {
        return ResponseEntity.ok(inventoryService.searchInventory(Long.valueOf(id)));
    }

    @PatchMapping("/{id}/edit")
    public ResponseEntity<?> editInventory(@PathVariable("id") Long id, @RequestParam(value = "name", required = false) String name, @RequestParam(value = "price", required = false) Double price, @RequestParam(value = "show", required = false) Boolean show){
        return ResponseEntity.ok(inventoryService.adjustment(id, name, price, show));
    }

    @PostMapping("/buy")
    public ResponseEntity<?> buyInventory(@RequestParam("inventoryId") Long inventoryId, @RequestParam("qty") Long qty){
        try {
            return ResponseEntity.ok(inventoryTransactionService.buy(inventoryId, qty));
        } catch (RecordNotFoundException | IllegalArgumentException e){
            return ResponseEntity.badRequest().body(e);
        }
    }

    @GetMapping("/usage")
    public ResponseEntity<?> listMyUsage(Authentication auth){
        UserPrincipal principal = ((UserPrincipal) auth.getPrincipal());
        return ResponseEntity.ok(inventoryTransactionService.myUsage(principal));
    }

    @GetMapping("/usage/all")
    public ResponseEntity<?> listAllUsage(){
        return ResponseEntity.ok(inventoryTransactionService.listAllUsage());
    }
}
