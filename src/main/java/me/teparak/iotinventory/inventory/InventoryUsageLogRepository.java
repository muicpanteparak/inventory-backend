package me.teparak.iotinventory.inventory;

import me.teparak.iotinventory.security.user.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface InventoryUsageLogRepository extends JpaRepository<InventoryUsageLog, Long> {
    <T> List<T> findByCreatedBy(UserAccount account, Class<T> projection);
    <T> List<T> findAllByCreatedByIsNotNull(Class<T> projection);
}
