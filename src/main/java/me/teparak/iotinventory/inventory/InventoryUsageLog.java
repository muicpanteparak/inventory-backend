package me.teparak.iotinventory.inventory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import me.teparak.iotinventory.common.AbstractRelationalAuditorEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Data
@Entity
@EqualsAndHashCode(callSuper = true)
@ToString(exclude = {"inventory"})
public class InventoryUsageLog extends AbstractRelationalAuditorEntity {


    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.REMOVE, CascadeType.REFRESH})
    private Inventory inventory;

    private Long qty;

    private Double broughtPrice;

    public InventoryUsageLog() {
    }
}
